package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "comment")
public class Comment {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column
	private Integer report_id;
	@Column
	private String content; // コメント内容

	// コメントIDを取得
	public int getId() {
		return id;
	}

	// コメントIDを格納
	public void setId(int id) {
		this.id = id;
	}



	public Integer getReport_id() {
		return report_id;
	}

	public void setReport_id(Integer report_id) {
		this.report_id = report_id;
	}

	public void setId(Integer report_id) {
		this.report_id = report_id;
	}

	// コメント内容を取得
	public String getContent() {
		return content;
	}

	// コメント内容を格納
	public void setContent(String content) {
		this.content = content;
	}
}
